package com.example.what_you_eat_app.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.what_you_eat_app.R

//project started from 30.03.2024 11:59 by Saburov Evgeniy Vladimirovich

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}